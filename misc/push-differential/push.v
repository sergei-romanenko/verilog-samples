module push (
    input clk,   // 12 MHz
    output wire led1,
    output wire led2,
    output wire led3,
    output wire led4,
    output wire led5,
    output wire led6,
    output wire led7,
    output wire led8,
    input b_in,
    output b_out
    );

    assign led2 = 1'b0;
    assign led3 = 1'b0;
    assign led4 = 1'b0;
    assign led5 = 1'b0;
    assign led6 = 1'b0;
    assign led7 = 1'b0;
    assign led8 = 1'b0;

    /* LED register */
    reg ledval = 1'b0;

    /* Numpad pull-up settings for columns:
       PIN_TYPE: <output_type=0>_<input=1>
       PULLUP: <enable=1>
       PACKAGE_PIN: <user pad name>
       D_IN_0: <internal pin wire (data in)>
    */
    wire b_in_din;
    SB_IO #(
        .PIN_TYPE(6'b0000_01),
        .PULLUP(1'b1)
    ) b_in_config (
        .PACKAGE_PIN(b_in),
        .D_IN_0(b_in_din)
    );

    assign b_out = 1'b0;
    /* LED Wiring */
    assign led1=ledval;

    /* Toggle LED when button [1] pressed */
    always @ (negedge b_in_din) begin
        ledval = ~ledval;
    end

endmodule
