module push (
    input clk,   // 12 MHz
    output wire led1,
    output wire led2,
    output wire led3,
    output wire led4,
    output wire led5,
    output wire led6,
    output wire led7,
    output wire led8,
    input wire b_in
    );

    assign led2 = 1'b0;
    assign led3 = 1'b0;
    assign led4 = 1'b0;
    assign led5 = 1'b0;
    assign led6 = 1'b0;
    assign led7 = 1'b0;
    assign led8 = 1'b0;

    /* LED register */
    reg ledval = 1'b0;

    /* LED Wiring */
    assign led1=ledval;

    always @ (posedge clk) begin
        ledval = b_in;
    end


endmodule
