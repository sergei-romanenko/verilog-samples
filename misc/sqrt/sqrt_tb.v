`include "sqrt.v"

`default_nettype none

module sqrt_tb;

reg clk, reset;
reg [31:0] data[4:0];
reg [31:0] x;
wire [15:0] y;
wire valid;

sqrt dut (clk, reset, x, valid, y);

always #10 clk = ~clk;

integer i;
initial begin
  $dumpfile("sqrt_tb.vcd");
  $dumpvars(0, sqrt_tb);

  /* Load the data set from the hex file. */
  $readmemh("sqrt.mem", data, 0, 4);
  for (i = 0 ;  i <= 4 ;  i = i + 1) begin
    clk = 0;
    reset = 1;

    x = data[i];

    #35 reset = 0;

    wait (valid) $display("x=%d, y=%d", x, y);
  end
  $finish;
end

endmodule
