`include "debouncer.v"
`include "pwm.v"

`default_nettype none

module pwm_tester #(
  parameter nleds = 8
)(
  input wire clk,
  input wire btn_incr,
  input wire btn_decr,
  output wire [nleds-1:0] leds
);

assign leds[nleds-1:1] = 0;

wire incr_up, decr_up;
debouncer d1(.clk (clk), .btn_input (btn_incr), .trans_up (incr_up));
debouncer d2(.clk (clk), .btn_input (btn_decr), .trans_up (decr_up));

reg [7:0] duty = 0;

pwm p(.clk(clk), .duty(duty), .pwm_pin(leds[0]));

always @(posedge clk)
begin
  if (incr_up)
    duty <= (duty << 1) + 1;
  if (decr_up)
    duty <= duty >> 1;
end

endmodule
