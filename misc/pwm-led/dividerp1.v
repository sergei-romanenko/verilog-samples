`ifndef _dividerp1_
`define _dividerp1_

`include "divider.vh"

`default_nettype none

module dividerp1 #(
  parameter M = `T_100ms
)(
  input wire clk,
  output wire tick
);

localparam N = $clog2(M);

reg [N-1:0] cnt = 0;

always @(posedge clk)
  cnt <= (cnt == M - 1) ? 0 : cnt + 1;

assign tick = (cnt == 0);

endmodule

`endif
