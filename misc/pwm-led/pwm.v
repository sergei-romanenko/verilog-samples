`ifndef _pwm_
`define _pwm_

`default_nettype none

`include "divider.vh"
`include "dividerp1.v"

module pwm #(
  parameter DELAY = `T_10ms / 256
)(
  input wire clk,
  input wire [7:0] duty,
  output reg pwm_pin
);

wire tick;

dividerp1 #(DELAY)
  DIV0 (.clk(clk), .tick(tick));

reg [7:0] cnt = 0;
always @(posedge clk)
  if (tick)
  begin
    if (cnt == 254)
      cnt <= 0;
    else
      cnt <= cnt + 1;
    pwm_pin <= (cnt < duty);
  end

endmodule

`endif
