`ifndef _debouncer_
`define _debouncer_

`default_nettype none

module debouncer(
  input wire clk,
  input wire btn_input,
  output reg state,
  output wire trans_up,
  output wire trans_dn
);

// Synchronize the switch input to the clock
reg sync_0, sync_1;

// Debounce the switch
reg [16:0] count = 0;
// true when all bits of count are 1's
wire finished = &count;

wire transition = (state != sync_1);

assign trans_dn = transition & finished & ~state;
assign trans_up = transition & finished & state;

always @(posedge clk)
begin
  sync_0 <= btn_input;
  sync_1 <= sync_0;
end

always @(posedge clk)
begin
  if (~transition)
    count <= 0;
  else
  begin
    count <= count + 1;
    if (finished)
      state <= ~state;
  end
end

endmodule

`endif
