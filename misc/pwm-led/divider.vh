// == Durations
// -- In seconds
`define T_1s     12000000
`define T_2s     24000000

//-- In milliseconds
`define T_500ms  6000000
`define T_250ms  3000000
`define T_100ms  1200000
`define T_10ms   120000
`define T_5ms    60000
`define T_2ms    24000
`define T_1ms    12000

// MHz
`define F_6MHz 2
`define F_4MHz 3
`define F_3MHz 4
`define F_2MHz 6
`define F_1MHz 12

// KHz
`define F_6KHz 2_000
`define F_4KHz 3_000
`define F_3KHz 4_000
`define F_2KHz 6_000
`define F_1KHz 12_000

// Hz
`define F_2Hz   6_000_000
`define F_1Hz   12_000_000
