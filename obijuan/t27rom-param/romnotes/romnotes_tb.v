`include "../../lib/divider.vh"
`include "../../lib/dividerp1.v"
`include "../lib/genrom.v"
`include "../lib/notegen.v"
`include "romnotes.v"

`default_nettype none

module romnotes_tb();

reg clk = 0;

wire ch_out;

romnotes #(.DUR(2))
  dut(
    .clk(clk),
    .ch_out(ch_out)
  );

always
  # 1 clk <= ~clk;

initial begin

  $dumpfile("romnotes_tb.vcd");
  $dumpvars(0, romnotes_tb);

  # 200 $display("END of simulation");
  $finish;
end

endmodule
