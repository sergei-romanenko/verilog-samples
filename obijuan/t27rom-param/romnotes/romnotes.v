`ifndef _romnotes_
`define _romnotes_

`include "../../lib/divider.vh"
`include "../../lib/dividerp1.v"
`include "../lib/genrom.v"
`include "../lib/notegen.v"

`default_nettype none

module romnotes #(
  parameter DUR = `T_200ms,
  parameter ROMFILE = "imperial.mem",
  parameter AW = 6,
  parameter DW = 16
)(
  input wire clk,
  output wire [7:0] leds,
  output wire ch_out
);

wire ch0, ch1, ch2;
reg [AW-1: 0] addr = 0;
wire clk_dur;
reg rstn = 0;
wire [DW-1: 0] note;

assign leds = note[7:0];

genrom #( .ROMFILE(ROMFILE), .AW(AW), .DW(DW))
  ROM (.clk(clk), .addr(addr), .data(note));

notegen
  CH0 (.clk(clk), .rstn(rstn), .note(note), .clk_out(ch_out));

always @(posedge clk)
  rstn <= 1;

always @(posedge clk)
  if (clk_dur)
    addr <= addr + 1;

dividerp1 #(DUR)
  TIMER0 (.clk(clk), .tick(clk_dur));

endmodule

`endif
