`ifndef _genromleds_
`define _genromleds_

`include "../../lib/divider.vh"
`include "../../lib/dividerp1.v"
`include "../lib/genrom.v"

`default_nettype none

module genromleds #(
  parameter DELAY = `T_500ms,
  parameter ROMFILE = "../lib/rom1.mem",
  parameter AW = 5,
  parameter DW = 5
)(
  input wire clk,
  output wire [7:0] leds
);

reg [AW-1: 0] addr = 0;

wire clk_delay;

genrom
  #(.ROMFILE(ROMFILE), .AW(AW), .DW(DW))
  ROM (.clk(clk), .addr(addr), .data(leds[4:0]));

assign leds[7:5] = 0;

always @(negedge clk)
  if (clk_delay)
    addr <= addr + 1;

dividerp1 #(.DELAY(DELAY))
  DIV0 ( .clk(clk), .tick(clk_delay));

endmodule

`endif
