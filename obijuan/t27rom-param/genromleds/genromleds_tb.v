`default_nettype none

`include "genromleds.v"

module genromleds_tb();

localparam DELAY = 2;
localparam ROMFILE = "../lib/rom1.mem";

reg clk = 0;

wire [7:0] leds8;
wire [4:0] leds = leds8[4:0];

genromleds #(.DELAY(DELAY), .ROMFILE(ROMFILE))
  dut (.clk(clk), .leds(leds8));

always #1 clk = ~clk;

initial begin

  $dumpfile("genromleds_tb.vcd");
  $dumpvars(0, genromleds_tb);

  # 140 $display("Stop!");
  $finish;
end

endmodule
