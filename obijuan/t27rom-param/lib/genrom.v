`ifndef _genrom_
`define _genrom_

`default_nettype none

module genrom #(
  parameter AW = 5, parameter DW = 4,
  parameter ROMFILE = "../lib/rom1.mem"
)(
  input clk,
  input wire [AW-1: 0] addr,
  output reg [DW-1: 0] data
);

localparam NPOS = 2 ** AW;

reg [DW-1: 0] rom [0: NPOS-1];

always @(posedge clk) begin
  data <= rom[addr];
end

initial begin
  $readmemh(ROMFILE, rom);
end

endmodule

`endif
