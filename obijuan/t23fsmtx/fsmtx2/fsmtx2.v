//-- The character "A" is sent every 100ms

`ifndef _fsmts2_
`define _fsmts2_

`default_nettype none

`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"
`include "../../lib/divider.vh"
`include "../../lib/dividerp1.v"

module fsmtx2 #(
  parameter BAUD =  `B115200,
  parameter CHAR = "A",
  parameter DELAY = `T_100ms
)(
  input wire clk,
  output reg tx
);

// === Microinstructions

wire load;
wire baud_enable;

// == Baudrate ticks

wire baud_tick;

baudgen #(BAUD)
  BAUD0 (.clk(clk), .enable(baud_enable), .tick(baud_tick));

// Slow ticks

wire slow_tick;

dividerp1 #(.DELAY(DELAY))
  DIV0 (.clk(clk), .tick(slow_tick));

// === Data routing

reg [9:0] shifter = 10'b11_1111_1111;

always @(posedge clk)
  if (load)
    shifter <= {CHAR, 2'b01};
  else if (!load && baud_tick)
    shifter <= {1'b1, shifter[9:1]};

reg [3:0] bitc;

always @(posedge clk)
  if (load == 1)
    bitc <= 0;
  else if (load == 0 && baud_tick == 1)
    bitc <= bitc + 1;

// Send a bit to tx.
// Since `shifter` is a register, there will be no spurious signals.
always @(posedge clk)
  tx <= shifter[0];

// === Controller

reg start = 0;

always @(posedge clk)
  start <= slow_tick;

// States
localparam IDLE = 0;
localparam START = 1;
localparam TRANS = 2;

reg [1:0] state = IDLE;

always @(posedge clk)
  case (state)

    IDLE:
      if (start)
        state <= START;
      else
        state <= IDLE;

    START:
      state <= TRANS;

    TRANS:
      if (bitc == 11)
        state <= IDLE;
      else
        state <= TRANS;

    // Never used
    default:
      state <= IDLE;

  endcase

// === Generating the microinstructions
assign load = (state == START);
assign baud_enable = (state != IDLE);

endmodule

`endif
