`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"
`include "../../lib/dividerp1.v"
`include "fsmtx2.v"

module fsmtx2_tb();

localparam BAUD = `B115200;
localparam DELAY = `F_8KHz;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 11);
localparam FRAME_WAIT = (BITRATE * 4);

reg clk = 0;

always
  # 1 clk <= ~clk;

wire tx;

fsmtx2 #(.BAUD(BAUD), .DELAY(DELAY))
  dut(
    .clk(clk),
    .tx(tx)
  );

initial begin

  $dumpfile("fsmtx2_tb.vcd");
  $dumpvars(0, fsmtx2_tb);

  #(FRAME_WAIT * 10) $display("END of simulation");
  $finish;
end

endmodule
