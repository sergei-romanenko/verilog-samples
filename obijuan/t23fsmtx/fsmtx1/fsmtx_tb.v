
`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"
`include "fsmtx.v"

`default_nettype none

module fsmtx_tb();

localparam BAUD = `B115200;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 11);
localparam FRAME_WAIT = (BITRATE * 4);

reg clk = 0;
wire tx;
reg start = 0;

fsmtx #(.BAUD(BAUD))
  dut(
    .clk(clk),
    .dtr(start),
    .tx(tx)
  );

always
  # 1 clk <= ~clk;

initial begin

  $dumpfile("fsmtx_tb.vcd");
  $dumpvars(0, fsmtx_tb);

  #1 start <= 0;

  #FRAME_WAIT start <= 1;
  #(BITRATE * 2) start <=0;

  #(FRAME_WAIT * 2) start <=1;
  #(FRAME * 1) start <=0;

  #(FRAME_WAIT * 4) $display("END of simulation");
  $finish;
end

endmodule
