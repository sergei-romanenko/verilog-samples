`ifndef _fsmtx_
`define _fsmtx_

`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"

`default_nettype none

module fsmtx #(parameter BAUD =  `B300, parameter CHAR = "A")(
  input wire clk,
  input wire dtr,
  output reg tx
);

// === Microinstructions

wire load;
wire baud_enable;

// == Baudrate ticks

wire baud_tick;

baudgen #(BAUD)
  BAUD0 (
    .clk(clk),
    .enable(baud_enable),
    .tick(baud_tick)
  );

// === Data routing

reg [9:0] shifter = 10'b11_1111_1111;

always @(posedge clk)
  if (load)
    shifter <= {CHAR, 2'b01};
  else if (baud_tick)
    shifter <= {1'b1, shifter[9:1]};

reg [3:0] bitc;

always @(posedge clk)
  if (load)
    bitc <= 0;
  else if (baud_tick)
    bitc <= bitc + 1;

// Send a bit to tx.
// Since `shifter` is a register, there will be no spurious signals.
always @(posedge clk)
  tx <= shifter[0];

// === Controller

reg start = 0;

always @(posedge clk)
  start <= dtr;

// States
localparam IDLE = 0;
localparam START = 1;
localparam TRANS = 2;

reg [1:0] state = IDLE;

always @(posedge clk)
  case (state)

    IDLE:
      if (start)
        state <= START;
      else
        state <= IDLE;

    START:
      state <= TRANS;

    TRANS:
      if (bitc == 11)
        state <= IDLE;
      else
        state <= TRANS;

    // Never used
    default:
      state <= IDLE;

  endcase

// === Generating the microinstructions
assign load = (state == START);
assign baud_enable = (state != IDLE);

endmodule

`endif
