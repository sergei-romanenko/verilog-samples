`include "../lib/divider.vh"
`include "../lib/divider.v"
`include "tones.v"

module tones_tb();

reg clk = 0;

wire ch0, ch1, ch2, ch3;

tones #(3, 5, 7, 10)
  dut(.clk(clk), .ch0(ch0), .ch1(ch1), .ch2(ch2), .ch3(ch3));

always
  # 1 clk <= ~clk;

initial begin

  $dumpfile("tones_tb.vcd");
  $dumpvars(0, tones_tb);

  # 100 $display("Stop!");
  $finish;
end

endmodule
