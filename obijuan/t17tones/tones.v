`ifndef _tones_
`define _tones_

`include "../lib/divider.vh"
`include "../lib/divider.v"

module tones(input wire clk, output wire ch0, ch1, ch2, ch3);

parameter F0 = `F_1KHz;
parameter F1 = `F_2KHz;
parameter F2 = `F_3KHz;
parameter F3 = `F_4KHz;

divider #(F0)
  CH0 (.clk_in(clk), .clk_out(ch0));

divider #(F1)
  CH1 (.clk_in(clk), .clk_out(ch1)
  );

divider #(F2)
  CH2 (.clk_in(clk), .clk_out(ch2));

divider #(F3)
  CH3 (.clk_in(clk), .clk_out(ch3));

endmodule

`endif
