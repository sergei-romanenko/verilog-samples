`include "divM.v"

module divM_tb();

reg clk = 0;
wire clk_out;

divM #(6)
  dut(
    .clk_in(clk),
    .clk_out(clk_out)
  );

always #1 clk = ~clk;

initial begin

  $dumpfile("divM_tb.vcd");
  $dumpvars(0, divM_tb);

  # 40 $display("Stop!");
  $finish;
end

endmodule
