`ifndef _divM_
`define _divM_

module divM(input wire clk_in, output wire clk_out);

// The clock is supposed to be at 12MHz.
parameter M = 12_000_000;
localparam N = $clog2(M);

reg [N-1:0] cnt = 0;

always @(posedge clk_in)
  if (cnt == M - 1)
    cnt <= 0;
  else
    cnt <= cnt + 1;

assign clk_out = cnt[N-1];

endmodule

`endif
