`include "counter.v"

// This module is not really necessary, and has been introduced
// just to show the instantiation of another module.

module top(input clk, output wire [NLEDS-1:0] leds);

localparam WIDTH = 30;
localparam NLEDS = 8;

counter #(WIDTH, NLEDS) counter_inst (.clk(clk), .leds(leds));

endmodule
