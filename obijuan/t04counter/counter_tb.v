`include "counter.v"

module counter_tb();

localparam WIDTH = 6;
localparam NLEDS = 4;

reg clk = 0;

wire [NLEDS-1:0] leds;

reg [WIDTH-1:0] cnt = 0;
wire [NLEDS-1:0] high = cnt[WIDTH-1:WIDTH-NLEDS];

counter #(WIDTH, NLEDS) counter_inst (
  .clk(clk),
  .leds(leds)
);

always #1 clk = ~clk;

always @(posedge clk) begin
  cnt <= cnt + 1;
end

always @(negedge clk) begin
  if (high != leds)
    $display("-->ERROR!. Expected: %d. Given: %d", high, leds);
end

initial begin

  $dumpfile("counter_tb.vcd");
  $dumpvars(0, counter_tb);
  $display("Start!");

  # 0.5 if (cnt != 0)
          $display("ERROR! cnt is not 0!");
        else
          $display("counter has been initialized. OK.");

  # 50 $display("Finish!");
  $finish;
end

endmodule
