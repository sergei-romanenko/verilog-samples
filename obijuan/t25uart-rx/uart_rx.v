`ifndef _uart_rx_
`define _uart_rx_

`include "../../lib/baudgen_rx.v"

`default_nettype none

module uart_rx #(
  parameter BAUD = 104 //B115200
)(
  input wire clk,
  input wire rx,
  output reg rcv,
  output reg [7:0] data = 0
);

// === Microinstructions
reg baud_enable;
reg clear;
reg load;

// == Baudrate ticks

wire baud_tick;

baudgen_rx #(.BAUD(BAUD))
  baudgen0 (.clk(clk), .enable(baud_enable), .tick(baud_tick));

// === Data routing

reg rx_r;

always @(posedge clk)
  rx_r <= rx;

reg [3:0] bitc;

always @(posedge clk)
  if (clear)
    bitc <= 0;
  else if (baud_tick)
    bitc <= bitc + 1;

reg [9:0] raw_data;

always @(posedge clk)
  if (baud_tick) begin
    raw_data = {rx_r, raw_data[9:1]};
  end

always @(posedge clk)
  if (load)
    data <= raw_data[8:1];

// === Controller

// States

localparam IDLE = 0;
localparam RECV = 1;
localparam LOAD = 2;
localparam DAV =  3;

reg [1:0] state = IDLE;

always @(posedge clk)
  case (state)

    IDLE :
      if (rx_r == 0)
        state <= RECV;
      else
        state <= IDLE;

    RECV:
      if (bitc == 10)
        state <= LOAD;
      else
        state <= RECV;

    LOAD:
      state <= DAV;

    DAV:
      state <= IDLE;

  default:
    state <= IDLE;
  endcase

// === Generating the microinstructions
always @* begin
  baud_enable = (state == RECV);
  clear = (state == IDLE);
  load = (state == LOAD);
  rcv = (state == DAV);
end

endmodule

`endif
