// Echoing of the received characters.

`ifndef _echo_
`define _echo_

`include "../../lib/baudgen.vh"
`include "../uart_tx.v"
`include "../uart_rx.v"

`default_nettype none

module echo(
  input wire clk,
  input wire rx,
  output wire tx,
  output wire [7:0] leds
);

localparam BAUD = `B115200;

wire rcv;
wire [7:0] data;
wire ready;

uart_rx #(BAUD)
  RX0 (.clk(clk), .rx(rx), .rcv(rcv), .data(data));

uart_tx #(BAUD)
  TX0 (.clk(clk), .start(rcv), .data(data), .tx(tx), .ready(ready));

assign leds = data;

endmodule

`endif
