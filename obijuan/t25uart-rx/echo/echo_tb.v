`include "../../lib/baudgen.vh"
`include "echo.v"

module echo_tb();

localparam BAUD = `B115200;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 10);
localparam FRAME_WAIT = (BITRATE * 4);

task send_char;
  input [7:0] char;
begin
  rx <= 0;
  #BITRATE rx <= char[0];
  #BITRATE rx <= char[1];
  #BITRATE rx <= char[2];
  #BITRATE rx <= char[3];
  #BITRATE rx <= char[4];
  #BITRATE rx <= char[5];
  #BITRATE rx <= char[6];
  #BITRATE rx <= char[7];
  #BITRATE rx <= 1;
  #BITRATE rx <= 1;
end
endtask

reg clk = 0;
always
  # 1 clk <= ~clk;

reg rx = 1;
wire tx;

echo #(BAUD)
  dut(.clk(clk), .rx(rx), .tx(tx));

initial begin

  $dumpfile("echo_tb.vcd");
  $dumpvars(0, echo_tb);

  #BITRATE      send_char(8'h55);
  #(FRAME_WAIT*3) send_char("K");

  #(FRAME_WAIT*4) $display("END of the simulation");
  $finish;
end

endmodule
