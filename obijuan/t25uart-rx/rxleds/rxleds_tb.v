`include "../../lib/baudgen.vh"
`include "rxleds.v"

`default_nettype none

module rxleds_tb();

localparam BAUD = `B115200;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 10);
localparam FRAME_WAIT = (BITRATE * 4);

reg clk = 0;

always
  # 1 clk <= ~clk;

reg rx = 1;
wire [7:0] leds;

rxleds #(BAUD)
  dut(.clk(clk), .rx(rx), .leds(leds));

task send_char;
  input [7:0] char;
begin
  rx <= 0;
  #BITRATE rx <= char[0];
  #BITRATE rx <= char[1];
  #BITRATE rx <= char[2];
  #BITRATE rx <= char[3];
  #BITRATE rx <= char[4];
  #BITRATE rx <= char[5];
  #BITRATE rx <= char[6];
  #BITRATE rx <= char[7];
  #BITRATE rx <= 1;
  #BITRATE rx <= 1;
end
endtask

initial begin

  $dumpfile("rxleds_tb.vcd");
  $dumpvars(0, rxleds_tb);

  #BITRATE    send_char(8'h55);
  #FRAME_WAIT send_char("K");

  #(FRAME_WAIT*4) $display("END of the simulation");
  $finish;
end

endmodule
