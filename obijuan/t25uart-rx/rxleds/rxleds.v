// The received characters are stored in a register and the least
// significant bits are displayed by the LEDs.

`ifndef _rxleds_
`define _rxleds_

`include "../../lib/baudgen.vh"
`include "../uart_rx.v"

`default_nettype none

module rxleds (
  input wire clk,
  input wire rx,
  output reg [7:0] leds = 0
);

localparam BAUD = `B115200;

wire rcv;
wire [7:0] data;

uart_rx #(BAUD)
  RX0 (.clk(clk), .rx(rx), .rcv(rcv), .data(data));

always @(posedge clk)
    if (rcv)
      leds <= data[7:0];

endmodule

`endif
