`ifndef _uart_tx_
`define _uart_tx_

`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"

`default_nettype none

module uart_tx #(
  parameter BAUD = `B115200
)(
  input wire clk,
  input wire start,
  input wire [7:0] data,
  output reg tx,
  output wire ready
);

// === Microinstructions

wire load;
wire baud_enable;

// == Baudrate ticks

wire baud_tick;

baudgen #(.BAUD(BAUD))
  BAUD0 (.clk(clk), .enable(baud_enable), .tick(baud_tick));

// === Data routing

reg [7:0] data_r;
reg [9:0] shifter = 10'b11_1111_1111;

always @(posedge clk)
  if (load)
    shifter <= {data_r, 2'b01};
  else if (baud_tick)
    shifter <= {1'b1, shifter[9:1]};

reg [3:0] bitc;

always @(posedge clk)
  if (load)
    bitc <= 0;
  else if (baud_tick)
    bitc <= bitc + 1;

// Send a bit to tx.
// Since `shifter` is a register, there will be no spurious signals.
always @(posedge clk)
  tx <= shifter[0];

// === Controller

reg start_r = 0;

always @(posedge clk)
  start_r <= start;

// States
localparam IDLE  = 0;
localparam START = 1;
localparam TRANS = 2;

reg [1:0] state = IDLE;

always @(posedge clk)
  case (state)

    IDLE:
      if (start_r)
        state <= START;
      else
        state <= IDLE;

    START:
      state <= TRANS;

    TRANS:
      if (bitc == 11)
        state <= IDLE;
      else
        state <= TRANS;

    // Never used
    default:
      state <= IDLE;

  endcase

// === Generating the microinstructions
assign load = (state == START);
assign baud_enable = (state != IDLE);

assign ready = (state == IDLE);

always @(posedge clk)
  if (start && state == IDLE)
    data_r <= data;

endmodule

`endif
