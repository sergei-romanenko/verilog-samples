// The string "Hello!.." is sent once a second

`ifndef _txstr2_
`define _txstr2_

`include "../../lib/baudgen.vh"
`include "../../lib/divider.vh"
`include "../../lib/dividerp1.v"
`include "../uart_tx.v"

module txstr2 #(
  parameter BAUD = `B300,
  parameter DELAY = `T_1s
)(
  input wire clk,
  output wire tx
);

wire ready;

reg [7:0] data;
reg [7:0] data_r;

reg transmit_r;
wire transmit;

// === Microinstructions

reg next;
reg txchar;

uart_tx #(.BAUD(BAUD))
  TX0 (.clk(clk), .data(data_r), .start(txchar), .ready(ready), .tx(tx));

// === Data routing

reg [2:0] char_no = 0;

always @*
  case (char_no)
    3'd0: data = "H";
    3'd1: data = "e";
    3'd2: data = "l";
    3'd3: data = "l";
    3'd4: data = "o";
    3'd5: data = "!";
    3'd6: data = ".";
    3'd7: data = ".";
    default: data = ".";
  endcase

always @*
  data_r = data;

always @(posedge clk)
  if (next)
    char_no = char_no + 1;

always @(posedge clk)
  transmit_r <= transmit;

dividerp1 #(.DELAY(DELAY))
  DIV0 ( .clk(clk), .tick(transmit));

// === Controller

localparam IDLE = 0;
localparam TXCHR = 2'd1;
localparam NEXT = 2'd2;
localparam END = 3;

// === FSM

reg [1:0] state = IDLE;

always @(posedge clk)
  case (state)

    IDLE:
      if (transmit_r) state <= TXCHR;
      else state <= IDLE;

    TXCHR:
      if (ready) state <= NEXT;
      else state <= TXCHR;

    NEXT:
      if (char_no == 7) state <= END;
      else state <= TXCHR;

    END:
      if (ready) state <= IDLE;
      else state <= END;

    // Never used
    default:
       state <= IDLE;

  endcase

// == Generating the microinstructions

always @* begin
  txchar = (state == TXCHR);
  next = (state == NEXT);
end

endmodule

`endif
