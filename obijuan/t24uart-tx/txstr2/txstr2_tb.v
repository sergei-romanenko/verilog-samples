`include "../../lib/baudgen.vh"
`include "txstr2.v"

module txstr2_tb();

localparam BAUD = `B115200;
localparam DELAY = 10000;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 11);
localparam FRAME_WAIT = (BITRATE * 4);

reg clk = 0;
always
  # 1 clk <= ~clk;

wire tx;

txstr2 #(.BAUD(BAUD), .DELAY(DELAY))
  dut(
    .clk(clk),
    .tx(tx)
  );

initial begin

  $dumpfile("txstr2_tb.vcd");
  $dumpvars(0, txstr2_tb);

  #(FRAME * 20) $display("END of the simulation");
  $finish;
end

endmodule
