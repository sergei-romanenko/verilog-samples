// When DTR is on, continuously sending the string "Hello!.."

`ifndef _txstr1_
`define _txstr1_`

`include "../../lib/baudgen.vh"
`include "../uart_tx.v"

`default_nettype none

module txstr1 #(
  parameter BAUD = `B300
)(
  input wire clk,
  input wire dtr,
  output wire tx
);

wire ready;
reg [7:0] data;
reg transmit = 0;

// === Microinstructions

reg start;
reg next;

uart_tx #(.BAUD(BAUD))
  TX0 (.clk(clk), .data(data), .start(start), .ready(ready), .tx(tx));

// === Data routing

reg [2:0] char_no = 0;

always @*
  case (char_no)
    3'd0: data = "H";
    3'd1: data = "e";
    3'd2: data = "l";
    3'd3: data = "l";
    3'd4: data = "o";
    3'd5: data = "!";
    3'd6: data = ".";
    3'd7: data = ".";
  endcase

always @(posedge clk)
  transmit <= dtr;

always @(posedge clk)
  if (next)
    char_no = char_no + 1;

// === Controller

localparam IDLE = 0;
localparam START = 2'd1;
localparam NEXT = 2'd2;
localparam END = 3;

// === FSM

reg [1:0] state = IDLE;

always @(posedge clk)
  case (state)

    IDLE:
      if (transmit) state <= START;
      else state <= IDLE;

    START:
      if (ready) state <= NEXT;
      else state <= START;

    NEXT:
      if (char_no == 7) state <= END;
      else state <= START;

    END:
      if (ready) state <= IDLE;
      else state <= END;

    // Never used
    default:
       state <= IDLE;

  endcase

// == Generating the microinstructions

always @* begin
  start = (state == START);
  next = (state == NEXT);
end

endmodule

`endif
