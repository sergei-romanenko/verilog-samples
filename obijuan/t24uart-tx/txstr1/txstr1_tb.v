`include "../../lib/baudgen.vh"
`include "txstr1.v"

`default_nettype none

module txstr1_tb();

localparam BAUD = `B115200;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 11);
localparam FRAME_WAIT = (BITRATE * 4);

reg clk = 0;

always
  # 1 clk <= ~clk;

wire tx;
reg dtr = 0;

txstr1 #(.BAUD(BAUD))
  dut(.clk(clk), .dtr(dtr), .tx(tx));

initial begin

  $dumpfile("txstr1_tb.vcd");
  $dumpvars(0, txstr1_tb);

  #1 dtr <= 0;

  #FRAME_WAIT dtr <= 1;
  #(BITRATE * 2) dtr <=0;

  #(FRAME * 11) dtr <=1;
  #(BITRATE * 1) dtr <=0;

  #(FRAME * 11) $display("END of the simulation");
  $finish;
end

endmodule
