`include "echowire1.v"

module echowire1_tb();

reg dtr = 0;
reg rts = 0;
reg rx = 0;
wire tx;
wire [7:0] leds;

echowire1
  dut(.dtr(dtr), .rts(rts), .leds(leds), .tx(tx), .rx(rx));

always
  #2 dtr <= ~dtr;

always
  #3 rts = ~rts;

always
  #1 rx <= ~rx;

//-- Proceso al inicio
initial begin

  $dumpfile("echowire1_tb.vcd");
  $dumpvars(0, echowire1_tb);

  # 200 $display("END of simulation");
  $finish;
end

endmodule
