`ifndef _echowire1_
`define _echowire1_

module echowire1(
  input wire dtr,
  input wire rts,
  input wire rx,
  output wire tx,
  output wire [7:0] leds);

assign leds[0] = dtr;
assign leds[1] = rts;

assign leds[7:2] = 0;

assign tx = rx;

endmodule

`endif
