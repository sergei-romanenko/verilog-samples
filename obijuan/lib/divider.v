`ifndef _divider_
`define _divider_

module divider(input wire clk_in, output wire clk_out);

parameter DELAY = 104;
localparam W = $clog2(DELAY);

reg [W-1:0] cnt = 0;
assign clk_out = cnt[W-1];

always @(posedge clk_in)
  cnt <= (cnt == DELAY - 1) ? 0 : (cnt + 1);

endmodule

`endif
