`ifndef _baudgen_
`define _baudgen_

`default_nettype none

module baudgen #(
  parameter BAUD = 104 // B115200
)(
  input wire clk,
  input wire enable,
  output wire tick
);

localparam max = BAUD - 1;
localparam W = $clog2(BAUD);

reg [W-1:0] cnt = 0;

always @(posedge clk)
  if (enable)
    cnt <= (cnt == max) ? 0 : (cnt + 1);
  else
    cnt <= max;

assign tick = enable && (cnt == 0);

endmodule

`endif
