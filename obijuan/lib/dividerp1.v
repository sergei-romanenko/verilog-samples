`ifndef _dividerp1_
`define _dividerp1_

`default_nettype none

module dividerp1 #(
  parameter DELAY = 1200000 // T_100ms
)(
  input wire clk,
  output wire tick
);

localparam w = $clog2(DELAY);

reg [w-1:0] divcounter = 0;

always @(posedge clk)
  divcounter <= (divcounter == DELAY - 1) ? 0 : divcounter + 1;

assign tick = (divcounter == 0);

endmodule

`endif
