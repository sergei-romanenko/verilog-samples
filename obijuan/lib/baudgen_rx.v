// The positive pulse width is 1 clock cycle.

`ifndef _baudgen_rx_
`define _baudgen_rx_

`default_nettype none

module baudgen_rx#(
  parameter BAUD = 104 //B115200
)(
  input wire clk,
  input wire enable,
  output wire tick
);


localparam W = $clog2(BAUD);
localparam max = BAUD - 1;
// Value to generate pulse in the middle of the period
localparam half = (BAUD >> 1);

reg [W-1:0] cnt = 0;

always @(posedge clk)

  if (enable)
    cnt <= (cnt == max) ? 0 : cnt + 1;
  else
    cnt <= max;

assign tick = enable && (cnt == half);

endmodule

`endif
