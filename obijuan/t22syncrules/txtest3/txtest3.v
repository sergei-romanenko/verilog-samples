`ifndef _txtest3_
`define _txtest3_

`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"
`include "../../lib/divider.vh"
`include "../../lib/divider.v"

`default_nettype none

// The module that continuously sends a character.
// The output tx is in a register.
module txtest3 #(
  parameter BAUD =  `B300,
  parameter DELAY = `T_250ms
)(
  input wire clk,
  output reg tx = 1
);

wire slow_tick;
reg baud_enable = 0;

always @(posedge clk)
  baud_enable <= slow_tick;

reg [9:0] shifter;
wire baud_tick;

always @(posedge clk)
  if (!baud_enable)
    shifter <= {"K", 2'b01};
  else if (baud_enable && baud_tick)
    shifter <= {1'b1, shifter[9:1]};

always @(posedge clk)
  tx <= baud_enable ? shifter[0] : 1;

baudgen #(BAUD)
  BAUD0 (.clk(clk), .enable(baud_enable), .tick(baud_tick));

divider #(DELAY)
  DIV0 (.clk_in(clk), .clk_out(slow_tick));

endmodule

`endif
