`include "../../lib/divider.vh"
`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"
`include "txtest3.v"

module txtest3_tb();

localparam BAUD = `B115200;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 11);

reg clk = 0;
wire tx;

txtest3 #(.BAUD(BAUD), .DELAY(`F_3KHz))
  dut(.clk(clk), .tx(tx));

always
  # 1 clk <= ~clk;

initial begin

  $dumpfile("txtest3_tb.vcd");
  $dumpvars(0, txtest3_tb);

  #(FRAME * 10) $display("END of simulation!");

  $finish;
end

endmodule
