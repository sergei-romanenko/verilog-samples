`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"
`include "txtest2.v"

`default_nettype none

module txtest2_tb();

localparam BAUD = `B115200;
localparam BITRATE = (BAUD << 1);
localparam FRAME = (BITRATE * 11);
localparam FRAME_WAIT = (BITRATE * 4);

reg clk = 0;
wire tx;
reg dtr = 0;

txtest2 #(.BAUD(BAUD))
  dut(.clk(clk), .dtr(dtr), .tx(tx));

always
  # 1 clk <= ~clk;

initial begin

  $dumpfile("txtest2_tb.vcd");
  $dumpvars(0, txtest2_tb);

  #1 dtr <= 0;

  #FRAME_WAIT dtr <= 1;
  #(FRAME * 3) dtr <=0;

  #FRAME_WAIT dtr <=1;
  #(FRAME * 3) dtr <=0;

  #FRAME_WAIT $display("END of simulation!");
  $finish;
end

endmodule
