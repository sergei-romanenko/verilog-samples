`ifndef _txtest2_
`define _txtest2_

`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"

`default_nettype none

// The module that continuously sends a character when dtr is 1.
// The output tx is in a register.
module txtest2 #(
  parameter BAUD =  `B300
)(
  input wire clk,
  input wire dtr,
  output reg tx = 1
);

reg enable = 0;

always @(posedge clk)
  enable <= dtr;

reg [9:0] shifter;
wire tick;

always @(posedge clk)
  if (!enable)
    shifter <= {"K", 2'b01};
  else if (enable && tick)
    shifter <= {shifter[0], shifter[9:1]};

always @(posedge clk)
  tx <= enable ? shifter[0] : 1;

baudgen #(BAUD)
  BAUD0 (.clk(clk), .enable(enable), .tick(tick));

endmodule

`endif
