`ifndef _txtest_
`define _txtest_

`include "../../lib/baudgen.vh"
`include "../../lib/baudgen.v"

`default_nettype none

// The module that sends a character when dtr is 1.
// The output tx is in a register.
module txtest #(
  parameter BAUD =  `B300
)(
  input wire clk,
  input wire dtr,
  output reg tx
);

reg load;

always @(posedge clk)
  load <= dtr;

reg [9:0] shifter;
wire tick;

always @(posedge clk)
  if (!load)
    shifter <= {"K", 2'b01};
  else if (load && tick)
    shifter <= {1'b1, shifter[9:1]};

always @(posedge clk)
  tx <= load ? shifter[0] : 1;

baudgen #(BAUD)
  BAUD0 (.clk(clk), .enable(load), .tick(tick));

endmodule

`endif
